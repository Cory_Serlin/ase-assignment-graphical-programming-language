﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is the class that is responsible for user defined methods.
    /// It will take in any commands within a method block and store them so that they can be declared later
    /// </summary>
    public class usermethods
    {
        // instance data relating to the class
        private List<String> methodNames = new List<string>();
        private List<String> methodCommands = new List<string>();
        private int endMethodBlockNumber = 0;
        private string[] programSplit;

        //constructor for the usermethods
        public usermethods()
        {
        }

        /// <summary>
        /// This method will check to ensure the method block has been ended properly. 
        /// If so it will store the number where the end of the block was found.
        /// If not it will ignore the method declaration and execute any commands within that block
        /// </summary>
        /// <param name="program">the program run by the user</param>
        /// <param name="lineNoFound">the number where the start of the method block is</param>
        /// <returns></returns>
        public Boolean isEndMethodFound(String program, int lineNoFound)
        {
            Boolean isEndMethodFound = false;
            programSplit = program.Split(';');

            for (int i = lineNoFound - 1; i < programSplit.Length; i++)
            {
                if (programSplit[i].ToLower().Contains("endmethod"))
                {
                    isEndMethodFound = true;
                    endMethodBlockNumber = i;
                }
            }

            return isEndMethodFound;
        }

        /// <summary>
        /// This method is designed to store the method name the user has declared
        /// </summary>
        /// <param name="declaration">the method declaration</param>
        public void storeMethodName(String declaration)
        {
            String[] declarationSplit = declaration.Split(' ');
            methodNames.Add(declarationSplit[1]);
        }

        /// <summary>
        /// This will store all commands from within the method block and put it into the methodcommands arraylist
        /// </summary>
        /// <param name="lineNoFound">the number where the start of the method block is</param>
        public void storeMethodCommands(int lineNofound)
        {
            String commmandsToAdd = "";

            for (int i = lineNofound; i < endMethodBlockNumber; i++)
            {
                commmandsToAdd = commmandsToAdd + programSplit[i] + ";";
            }

            methodCommands.Add(commmandsToAdd);
        }

        /// <summary>
        /// Allows the user to get the method name they're after
        /// </summary>
        /// <returns>a method name at a given index</returns>
        public string getMethodNames(int index)
        {
            return methodNames[index];
        }

        /// <summary>
        /// Allows the user to get the list of commands they're after 
        /// </summary>
        /// <returns>a list of commands at a given index</returns>
        public string getMethodCommands(int index)
        {
            return methodCommands[index];
        }

        /// <summary>
        /// gets the number where the method block ends
        /// </summary>
        /// <returns>number where the endmethod was found</returns>
        public int getLineNoOfEndMethod()
        {
            return endMethodBlockNumber;
        }

        /// <summary>
        /// This will check the command passed to it to see if it is calling a method
        /// </summary>
        /// <param name="Command">the command passed to it</param>
        /// <returns>true if the command is a method call</returns>
        public Boolean isCommandAMethodCall(String Command)
        {
            return methodNames.Contains(Command);
        }
        
        /// <summary>
        /// This will get the indexvalue of the found method
        /// </summary>
        /// <param name="methodName"></param>
        /// <returns></returns>
        public int getMethodNameValue(String methodName)
        {
            return methodNames.FindIndex(a => a.Contains(methodName));
        }

        /// <summary>
        /// This will execute the method that has been called. 
        /// It will iterate through every commmand and try to execute the commmand.
        /// </summary>
        /// <catches>Argumentexception when the command entered does not exist</catches>
        public void methodexecution(String methodName,messageStorer messageStorer,commandParser parser,graphicsPen pen, Graphics output, setFill fill)
        {
            commandFactory factory = new commandFactory();
            String commandstoexecute = methodCommands[getMethodNameValue(methodName)];
            int methodLineNumber = 1;
            
            String[] commandstoexecuteSplit = commandstoexecute.Split(';');

            for(int i = 0; i < commandstoexecuteSplit.Length; i++)
            {
                messageStorer.addLineNumber(methodLineNumber);

                if(!String.IsNullOrEmpty(commandstoexecuteSplit[i]))
                {
                    parser.resetRawParamaters();
                    parser.parseCommandLine(commandstoexecuteSplit[i]);

                    if (parser.doParamatersExist())
                    {
                        parser.setRawParamaters();
                    }

                    if (parser.getTypedCommand().ToLower().Equals("setfill"))
                    {
                        fill.setFillExecution(parser, messageStorer);
                    }

                    else
                    {
                        try
                        {
                            Commands newCommand = factory.getCommands(parser.getTypedCommand(), messageStorer);
                            newCommand.executeCommand(parser, pen, output, fill, messageStorer);
                        }

                        catch (System.ArgumentException e)
                        {
                            messageStorer.addMessage(e.Message);
                        }

                    }
                }

                methodLineNumber++;
            }
        }
        
    }
}
