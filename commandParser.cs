﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class represents the commandParser.
    /// This class is responsible for taking the line the user enteres and separating it into two components:
    /// The first component is the typed command.
    /// The second component is the raw paramaters entered
    /// </summary>
    public class commandParser
    {
        //instance data for command parser
        private string[] commandLineSplit;
        private string typedCommand, rawParamaters;

        /// <summary>
        /// Constructor of the commandParser
        /// </summary>
        public commandParser()
        {

        }

        /// <summary>
        /// This method will reset the paramatersSplit into an empty array so that new paramaters can be stored.
        /// It will split the command entered into an array which is the command itself and the paramaters entered.
        /// It will also set the command the user has typed
        /// </summary>
        /// <param name="commandLine"> represents the command the user has entered</param>
        public void parseCommandLine(string commandLine)
        {
            commandLineSplit = commandLine.Trim().Split(' ');
            setTypedCommand(commandLineSplit[0]);
        }

        /// <summary>
        /// This will return the array which is storing the command and it's paramaters 
        /// </summary>
        /// <returns>An array representing the command and paramaters the user has passed to it</returns>
        public string[] getCommandLineSplit()
        {
            return commandLineSplit;
        }

        /// <summary>
        /// Sets the command the user has entered
        /// </summary>
        /// <param name="command"> represents the command the user inputs</param>
        public void setTypedCommand(string command)
        {
            this.typedCommand = command;
        }

        /// <summary>
        /// Getter for the command the user enters
        /// </summary>
        /// <returns>The typed command entered by the user</returns>
        public string getTypedCommand()
        {
            return typedCommand;
        }

        /// <summary>
        /// Checks if the user has typed any paramaters into the command line
        /// </summary>
        /// <returns>true if paramaters exist</returns>
        public Boolean doParamatersExist()
        {
            if (commandLineSplit.Length == 2)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// This resets the raw paramaters at the start of the command
        /// </summary>
        public void resetRawParamaters()
        {
            rawParamaters = null ;
        }

        /// <summary>
        /// This sets the raw paramaters the user has entered
        /// </summary>
        public void setRawParamaters()
        {
            rawParamaters = commandLineSplit[1];
        }

        /// <summary>
        /// This gets the raw paramaters the user enters
        /// </summary>
        /// <returns>The raw paramaters</returns>
        public string getRawParamaters()
        {
            return rawParamaters;
        }

    }
}
