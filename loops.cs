﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class is designed to handle the execution of loops. 
    /// </summary>
    public class loops
    {
        private string program;
        private int lineWhileFound,endWhile;
                
        /// <summary>
        /// This is the constructor for the loops command
        /// </summary>
        public loops()
        {

        }

        /// <summary>
        /// Gets where the while loop starts
        /// </summary>
        /// <returns>the number where the while loop starts</returns>
        public int getLineWhileFound()
        {
            return lineWhileFound;
        }

        /// <summary>
        /// Gets where the while loop ends
        /// </summary>
        /// <returns>the number where the while loop ends</returns>
        public int getEndWhile()
        {
            return endWhile;
        }

        /// <summary>
        /// This method loops through the program to find the end while in the program
        /// It will also call the number where the endwhile is found
        /// </summary>
        /// <returns>true if an endwhile is found</returns>
        public Boolean findEndWhile()
        {
            Boolean isEndWhilefound = false;
            string[] programSplit = program.Split(';');

            for (int i = 0; i < programSplit.Length; i++)
            {
                if(programSplit[i].ToLower().Contains("endwhile"))
                {
                    isEndWhilefound = true;
                    setEndWhileNo(i);
                }
            }

            return isEndWhilefound;
        }
        
        /// <summary>
        /// this sets the number where the endwhile was found
        /// </summary>
        /// <param name="foundNumber">the number where endwhile was found</param>
        public void setEndWhileNo(int foundNumber)
        {
            endWhile = foundNumber;
        }
                
        /// <summary>
        /// This sets the program to be processed by the loops class
        /// </summary>
        /// <param name="program">representing an instance of the program being executed by the user</param>
        public void setProgram(string program)
        {
            this.program = program;
        }
    
        /// <summary>
        /// This sets the line number where the while loop begins
        /// </summary>
        /// <param name="lineNo">number where the while loop begins</param>
        public void setLineNoWhileFound(int lineNo)
        {
            lineWhileFound = lineNo;
        }    

    }
}
