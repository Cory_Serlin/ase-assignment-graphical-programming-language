﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class handles the setpencolour command
    /// </summary>
    public class setPenColour : Commands
    {
        // string representing params
        private String[] paramaters;

        /// <summary>
        /// This is the method to execute the setPenColour command.
        /// It checks to ensure the user has entered the correct number of paramaters and that the paramater is a valid colour.
        /// If passes both checks, the command executes.
        /// If either check fails, then it produces an error relating to the check that failed.
        /// </summary>
        /// <param name="parser">The command parser used for command execution</param>
        /// <param name="pen">The graphical pen drawing on the graphics context</param>
        /// <param name="g">The graphical context of the drawing area</param>
        /// <param name="fill">The setFill command</param>
        /// <param name="syntax">The message handler that outputs any error messages</param>
        /// <catches>NullReferenceException when user enters no paramaters</catches>
        public override void executeCommand(commandParser parser, graphicsPen pen, Graphics g, setFill fill,messageStorer syntax)
        {
            try
            {
                base.setParamaters(parser.getRawParamaters());
                paramaters = base.getParamaters();

                if (base.isValidNoOfParamaters(1))
                {
                    if (isParamatersValid())
                    {
                        pen.setPenBrush(paramaters[0]);
                        syntax.addMessage("Pen colour changed to: " + paramaters[0]);
                    }

                    else
                    {
                        syntax.addMessage("invalid colour");
                    }
                }

                else
                {
                    syntax.addMessage("invalid number of params (1)");
                }
            }

            catch (System.NullReferenceException)
            {
                syntax.addMessage("No paramaters have been entered");
            }



        }

        /// <summary>
        /// This method checks if the paramater passed to it is a valid colour 
        /// </summary>
        /// <returns>true if the paramater passed to it is a known colour</returns>
        public override bool isParamatersValid()
        {
            if(Color.FromName(paramaters[0].Trim()).IsKnownColor)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
