﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class is responsible for storing messages produced from checking the program
    /// </summary>
    public class messageStorer
    {
        private List<string> errorList = new List<string>();
        private List<int> lineNumber = new List<int>();
                
        /// <summary>
        /// This is the constructor
        /// </summary>
        public messageStorer()
        {

        }
        
        /// <summary>
        /// This adds the line no of the command being processed
        /// </summary>
        /// <param name="lineNo">represents the line being executed</param>
        public void addLineNumber(int lineNo)
        {
            lineNumber.Add(lineNo);
        }
        
        /// <summary>
        /// This adds the message produced from the execution of a command 
        /// </summary>
        /// <param name="message">representing the status message of the line that was executed</param>
        public void addMessage(string message)
        {
            errorList.Add(message);
        }
    
        /// <summary>
        /// This iterates through the messages and outputs all of them
        /// </summary>
        /// <returns>a string representing all the messages produced from the execution of the program</returns>
        public string outputMessages()
        {
            String message = "";

            for(int i = 0; i < errorList.Count; i++)
            {
                message = message + "Line No: " + lineNumber[i] + " message: " + errorList[i] + Environment.NewLine;
            }
                        
            return message;
        }
    
        /// <summary>
        /// This resets the stored message list and line numbers 
        /// </summary>
        public void resetLineNoandMessages()
        {
            errorList.Clear();
            lineNumber.Clear();
        }
    
    
    
    }
}
