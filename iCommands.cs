﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is the interface for creating new commands for the user to use.
    /// </summary>
    interface iCommands
    {
        void setParamaters(string rawParamaters);
        void executeCommand(commandParser parser, graphicsPen pen, Graphics g, setFill fill, messageStorer syntax);
        Boolean isValidNoOfParamaters(int requiredNumber);
        Boolean isParamatersValid();
    }
}
