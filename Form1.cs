﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Assignment___Graphical_Programming_Language
{
    public partial class Form1 : Form
    {
        //Instance data of components relating to the program
        private commandParser commandParser;
        private graphicsPen graphicsArea;
        private turtleDisplay turtleDisplay;        
        private int graphicAreaHeight, graphicAreaWidth;
        private Bitmap outputBitmap, turtleBitmap;
        private Graphics outputGraphics, turtleGraphics;
        private setFill fill;
        private programArea programArea;
        private messageStorer syntaxChecker;

        /// <summary>
        /// This is the constructor for the form.
        /// It initialises all components relating to the program.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            commandParser = new commandParser();
            syntaxChecker = new messageStorer();

            graphicAreaHeight = drawingAreaPictureBox.Height;
            graphicAreaWidth = graphicAreaHeight = drawingAreaPictureBox.Width;
            outputBitmap = new Bitmap(graphicAreaWidth, graphicAreaHeight);
            turtleBitmap = new Bitmap(graphicAreaWidth, graphicAreaHeight);

            outputGraphics = Graphics.FromImage(outputBitmap);
            turtleGraphics = Graphics.FromImage(turtleBitmap);

            graphicsArea = new graphicsPen(outputGraphics);
            turtleDisplay = new turtleDisplay(turtleGraphics, graphicsArea);
            fill = new setFill();

            programArea = new programArea(commandParser, graphicsArea, outputGraphics, fill, syntaxChecker);

            turtleDisplay.drawInitialTurtle();
        }

        private void drawingAreaPictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            turtleDisplay.refreshTurtle();
            g.DrawImageUnscaled(outputBitmap, 0, 0);
            g.DrawImageUnscaled(turtleBitmap, 0, 0);
        }

        private void saveProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveEvent();
        }

        private void loadProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadEvent();
        }

        private void howToUseTheProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            howToUseMessage();
        }

        private void listOfCommandsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listOfCommandsMessage();
        }

        private void programInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This program was created by Cory Serlin", "Program Information");
        }

        private void runProgramButton_Click(object sender, EventArgs e)
        {
            consoleWindowTextBox.Clear();

            if (string.IsNullOrEmpty(programAreaTextBox.Text))
            {
                consoleWindowTextBox.AppendText("Error - Nothing has been entered in the program area");
            }

            else
            {
                runProgramEvent(programAreaTextBox.Text);
            }
        }

        private void checkProgramButton_Click(object sender, EventArgs e)
        {
            checkProgramEvent();
        }

        private void commandLineTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                consoleWindowTextBox.Clear();

                if(commandLineTextBox.Text.Equals("run", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (string.IsNullOrEmpty(programAreaTextBox.Text))
                    {
                        consoleWindowTextBox.AppendText("Error - Nothing has been entered in the program area");
                    }

                    else
                    {
                        runProgramEvent(programAreaTextBox.Text);
                    }
                }

                else if(string.IsNullOrEmpty(commandLineTextBox.Text))
                {
                    consoleWindowTextBox.AppendText("Error - Nothing has been entered in the command line");
                }

                else
                {
                    runProgramEvent(commandLineTextBox.Text);
                }
                
            }
        }

        /// <summary>
        /// This method outputs a message box detailing information on how to use this program
        /// </summary>
        public void howToUseMessage()
        {
            String Title = "How to use this program";
            String Howtheprogramworks = "This program is designed to produce graphical outputs based on the inputs you put into it";
            String HowToMakeProgramWork = "\nThis is done by entering inputs into the program area or command line";
            String WheretoFindCommands = "\n\nFor a list of commands you can use and how to type them out, press the list of commands menu item in the help menu";
            MessageBox.Show(Howtheprogramworks + HowToMakeProgramWork + WheretoFindCommands,Title);
        }
        
        /// <summary>
        /// This method outputs a message box detailing the commands a user can use
        /// </summary>
        public void listOfCommandsMessage()
        {
            String Title = "List of commands";
            String drawCircle = "drawcircle <radius>\n";
            String drawRectangle = "drawRectangle <width>,<height>\n";
            String drawTriangle = "drawTriangle <base>,<height>\n";
            String setPenColour = "setPenColour <colour>\n";
            String moveTo = "moveTo <X co-ordinate>,<Y co-ordinate>\n";
            String drawTo = "drawTo <X co-ordinate>,<Y co-ordinate>\n";
            String clearPanel = "clearpanel\n";
            String resetPanel = "resetpanel\n";
            String setFill = "setFill <on or off>";
            MessageBox.Show(drawRectangle + moveTo + setPenColour + drawCircle + drawTriangle + drawTo + clearPanel + resetPanel + setFill,
                Title);
        }
                
        /// <summary>
        /// This method handles the event when the user runs a program.
        /// </summary>
        /// <param name="programToRun">String representing the programming area or command line</param>
        public void runProgramEvent(string programToRun)
        {
            syntaxChecker.resetLineNoandMessages();
            programArea.parseProgram(programToRun);
            programArea.programExecution();
            consoleWindowTextBox.AppendText(syntaxChecker.outputMessages());
            Refresh();
        }
                
        /// <summary>
        /// This method handles the event when the user presses the check program button.
        /// </summary>
        public void checkProgramEvent()
        {
            syntaxChecker.resetLineNoandMessages();
            consoleWindowTextBox.Clear();
            programArea.parseProgram(programAreaTextBox.Text);
            programArea.checkProgram();
            outputGraphics.Clear(Color.White);
            graphicsArea.setPenX(0);
            graphicsArea.setPenY(0);
            turtleDisplay.refreshTurtle();
            consoleWindowTextBox.AppendText(syntaxChecker.outputMessages());
        }
                
        /// <summary>
        /// This method handles the event when the user presses the save program button.
        /// It will save the program into a '.txt' file.
        /// </summary>
        public void saveEvent()
        {
            SaveFileDialog saveFile = new SaveFileDialog();

            saveFile.DefaultExt = "*.txt";
            saveFile.Filter = "TXT Files|*.txt";

            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
               saveFile.FileName.Length > 0)
            {
                programAreaTextBox.SaveFile(saveFile.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        /// <summary>
        /// This method handles the event when the user presses the load program button.
        /// This will load a '.txt' file and write the file into the program area.
        /// The user is then able to run the program from there 
        /// </summary>
        public void loadEvent()
        {
            OpenFileDialog loadFile = new OpenFileDialog();

            loadFile.DefaultExt = "*.txt";
            loadFile.Filter = "TXT Files|*.txt";

            if (loadFile.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
               loadFile.FileName.Length > 0)
            {
                programAreaTextBox.LoadFile(loadFile.FileName, RichTextBoxStreamType.PlainText);
            }
        }

    }
}
