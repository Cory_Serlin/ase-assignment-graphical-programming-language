﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This method handles the turtle display - a visual representation of the user operated pen. 
    /// </summary>
    public class turtleDisplay
    {
        // instance data for the pen that will draw the "turtle" 
        private int xPos, yPos;
        private Pen myPen;

        // instance data for the graphical context of the "turtle"
        private Graphics turtleGraphics;

        // instance data of the GraphicsArea so that the "turtle" can match the position of the pen when drawing objects
        private graphicsPen myGraphicsArea;

        /// <summary>
        /// This is the constructor for the turtle display.
        /// It will set the position of the turtle to be the same position as the user operated pen.
        /// </summary>
        /// <param name="turtleOverlay"> this is an image that will be overlayed over the main drawing area so the user can see the "turtle"</param>
        /// <param name="drawingPen"> this will be the user operated drawingPen</param>
        public turtleDisplay(Graphics turtleOverlay, graphicsPen drawingPen)
        {
            this.turtleGraphics = turtleOverlay;
            this.myGraphicsArea = drawingPen;
            myPen = new Pen(Color.Red, 1);

            xPos = myGraphicsArea.getPenX();
            yPos = myGraphicsArea.getPenY();
        }

        /// <summary>
        /// This will draw the turtle in its starting position
        /// </summary>
        public void drawInitialTurtle()
        {
            turtleGraphics.DrawRectangle(myPen, xPos, yPos, 3, 3);
        }

        /// <summary>
        /// This will set the X co-ordinate of the turtle
        /// </summary>
        public void setXPos()
        {
            xPos = myGraphicsArea.getPenX();
        }

        /// <summary>
        /// This will set the Y co-ordinate of the turtle 
        /// </summary>
        public void setYPos()
        {
            yPos = myGraphicsArea.getPenY();
        }

        /// <summary>
        /// This class will clear the image the turtle is drawn on.
        /// It will then re-draw the turtle in its new position.
        /// </summary>
        public void refreshTurtle()
        {
            turtleGraphics.Clear(Color.Transparent);
            setXPos();
            setYPos();
            turtleGraphics.DrawRectangle(myPen, xPos, yPos, 3, 3);
        }


    }
}
