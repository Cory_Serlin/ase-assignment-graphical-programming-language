﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is the class that handles ifstatements and other conditionals. 
    /// </summary>
    public class ifStatements
    {
        //instance data of components relating to the execution of the command 
        private string statement, comparator, operation, condition;
        private string[] statementSplit;
        private Boolean equals = false;
        private Boolean lessThan = false;
        private Boolean moreThan = false;
        private Boolean lessORequal = false;
        private Boolean moreORequal = false;
        private variables var;
        private int foundEndif;

        /// <summary>
        /// This is the contructor for the command
        /// </summary>
        /// <param name="var">variables so the command can access stored variables</param>
        public ifStatements(variables var)
        {
            this.var = var;
        }

        /// <summary>
        /// This method loops through the program to find the end if in the program
        /// </summary>
        /// <param name="program">the program being processed</param>
        /// <param name="lineNo">the lineno where the if starts</param>
        /// <returns></returns>
        public Boolean findendif(string program, int lineNo)
        {
            Boolean isEndIfPresent = false;
            string[] programSplit = program.Split(';');

            for (int i = lineNo; i < programSplit.Length; i++)
            {
                if (programSplit[i].ToLower().Contains("endif"))
                {
                    isEndIfPresent = true;
                    setendifno(i);
                    break;
                }
            }

            return isEndIfPresent;
        }
        
        /// <summary>
        /// This sets the number where the end if was found
        /// </summary>
        /// <param name="findnumber">where the end if was found</param>
        public void setendifno(int findnumber)
        {
            foundEndif = findnumber;
        }

        /// <summary>
        /// This gets the interger for the line the end if was found
        /// </summary>
        /// <returns>an int representing the line the end if was found</returns>
        public int getendif()
        {
            return foundEndif;
        }
                
        /// <summary>
        /// This checks the comparator or condition and replaces its value with the value from an array.
        /// If that name exists in the array
        /// </summary>
        /// <param name="input">the comparator and the condition being assessed</param>
        /// <returns></returns>
        public string setValue(string input)
        {
            if (var.doesVarNameExist(input))
            {
                input = var.getVarValues(var.getVarNameIndex(input));
            }

            return input;
        }

        /// <summary>
        /// This will store the conditional statment 
        /// </summary>
        /// <param name="statement">represents the conditional statement</param>
        public void storeStatement(string statement)
        {
            this.statement = statement;
        }

        /// <summary>
        /// This will split the statement into 3 components 
        /// 1. Comparator - the thing thats being checked
        /// 2. Operation - the operator being used e.g. =
        /// 3. Condition - the condition being evaluated 
        /// </summary>
        /// <throws>conditionalformatexception when the user does format the statement correctly</throws>
        public void setConditionals()
        {
            statementSplit = statement.Trim().Split(' ');
            
            if(statementSplit.Length == 4)
            {
                comparator = statementSplit[1].Trim();
                operation = statementSplit[2].Trim();
                condition = statementSplit[3].Trim();
            }

            else
            {
                throw new conditionalformatexception("Conditional not formatted properly check it and try again");
            }

        }

        /// <summary>
        /// This checks the operator that was set and raises a flag representing which 
        /// operator was selected
        /// </summary>
        /// <throws>operatornotrecognised when the user doesn't enter a known operator</throws>
        public void processOperator()
        {
            equals = false;
            lessThan = false;
            moreThan = false;
            lessORequal = false;
            moreORequal = false;

            switch (operation)
            {
                case "=":
                    Console.WriteLine("Operator is =");
                    equals = true;
                    break;

                case "<":
                    Console.WriteLine("Operator is <");
                    lessThan = true;
                    break;

                case ">":
                    Console.WriteLine("Operator is >");
                    moreThan = true;
                    break;

                case "<=":
                    Console.WriteLine("Operator is <=");
                    lessORequal = true;
                    break;

                case ">=":
                    Console.WriteLine("Operator is >=");
                    moreORequal = true;
                    break;

                default:
                    throw new operatornotrecognised("Operator in conditional is not recognised please check it and try again");
                    
            }
        }

        
        /// <summary>
        /// This class evaluates the statement that was stored in this class
        /// </summary>
        /// <returns>true if the statement is true</returns>
        /// <returns>false if the statement is false</returns>
        public Boolean evaluateStatement()
        {
            comparator = setValue(comparator);
            condition = setValue(condition);

            Console.WriteLine(comparator);
            Console.WriteLine(condition);

            try
            {
                int.Parse(comparator);
            }
            
            catch (System.FormatException)
            {
                throw new System.FormatException("Comparator given is not an int");
            }

            try
            {
                int.Parse(condition);
            }

            catch (System.FormatException)
            {
                throw new System.FormatException("Condition given is not an int");
            }

            if (equals)
            {
                if (int.Parse(comparator) == int.Parse(condition))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else if (lessThan)
            {
                if (int.Parse(comparator) < int.Parse(condition))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else if (moreThan)
            {
                if (int.Parse(comparator) > int.Parse(condition))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else if (lessORequal)
            {
                if (int.Parse(comparator) <= int.Parse(condition))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else if (moreORequal)
            {
                if (int.Parse(comparator) >= int.Parse(condition))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }

        }

    }
}
