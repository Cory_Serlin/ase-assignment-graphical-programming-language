﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class handles the execution of drawRectangle 
    /// </summary>
    public class drawRectangle : Commands
    {
        private string[] paramaters;
        /// <summary>
        /// This is the method to execute the drawRectangle command.
        /// It checks to ensure the user has entered the correct number of paramaters and that the paramaters are valid intergers.
        /// If passes both checks, the command executes.
        /// If either check fails, then it produces an error relating to the check that failed.
        /// </summary>
        /// <param name="parser">The command parser used for command execution</param>
        /// <param name="pen">The graphical pen drawing on the graphics context</param>
        /// <param name="g">The graphical context of the drawing area</param>
        /// <param name="fill">The setFill command</param>
        /// <param name="syntax">The message handler that outputs any error messages</param>
        /// <catches>NullReferenceException when user enters no paramaters</catches>
        public override void executeCommand(commandParser parser, graphicsPen pen, Graphics g, setFill fill, messageStorer syntax)
        {
            try
            {
                base.setParamaters(parser.getRawParamaters());
                paramaters = base.getParamaters();

                if (base.isValidNoOfParamaters(2))
                {
                    if (base.isParamatersValid())
                    {
                        if (fill.getFillStatus())
                        {
                            g.FillRectangle(pen.GetBrush(), pen.getPenX(), pen.getPenY(), int.Parse(paramaters[0]), int.Parse(paramaters[1]));
                            syntax.addMessage("Command drawRectangle is valid.");
                        }

                        else
                        {
                            g.DrawRectangle(pen.GetPen(), pen.getPenX(), pen.getPenY(), int.Parse(paramaters[0]), int.Parse(paramaters[1]));
                            syntax.addMessage("Command drawRectangle is valid.");
                        }
                    }

                    else
                    {
                        syntax.addMessage("The paramater you have entered (" + paramaters[0] + ", " + paramaters[1] + ") is invalid please check the paramater and try again");
                    }
                }

                else
                {
                    syntax.addMessage("You have entered the wrong amount of paramaters. Please ensure you only have two paramaters entered");
                }
            }

            catch (System.NullReferenceException)
            {
                syntax.addMessage("No paramaters have been entered");
            }




        }
    }
}
