﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is the class that handles the execution of programs/commands inputted by the user 
    /// </summary>
    class programArea
    {
        //instance data relating components for command execution
        private commandParser commandParser;
        private graphicsPen graphicsArea;
        private Graphics outputGraphics;
        private setFill setFill;
        private commandFactory factory;
        private variables var;
        private ifStatements ifs;
        private loops loop;
        private messageStorer syntaxChecker;
        private usermethods usermethods;

        //instance data for storing the program
        string program;
        string[] programSplit;
        int lineNo = 1;
        int iterator = 0;

        //instance data for method execution
        private Boolean methodStored = false;
        private Boolean methodEnded = false;

        /// <summary>
        /// This is the constructor of the program.
        /// </summary>
        /// <param name="Parser">represents the commandparser created at launch of the program</param>
        /// <param name="pen">represents the graphicspen that is created at launch of the program</param>
        /// <param name="output">represents the graphics of what the user is drawing on which is made on launch</param>
        /// <param name="fill">represents the setfill command</param>
        /// <param name="syntax">represents the message storer which is created at launch</param>
        public programArea(commandParser Parser, graphicsPen pen, Graphics output, setFill fill, messageStorer syntax)
        {
            commandParser = Parser;
            graphicsArea = pen;
            outputGraphics = output;
            setFill = fill;
            factory = new commandFactory();    
            loop = new loops();
            syntaxChecker = syntax;
            var = new variables(syntax);
            ifs = new ifStatements(var);
            usermethods = new usermethods();
        }

        /// <summary>
        /// This method stores the program or command that the user enters in the program
        /// </summary>
        /// <param name="program">representing the input from the program area or command line </param>
        public void parseProgram(string program)
        {
            this.program = program;
        }

        /// <summary>
        /// This is the method that handles the execution of a program or command that is passed to it.
        /// It splits the program into an array and iterates through each line.
        /// It then calls the appropriate method based on what is inputted into it. 
        /// </summary>
        public void programExecution()
        {
            lineNo = 1;
            programSplit = program.Split(';');

            for (iterator = 0; iterator < programSplit.Length; iterator++)
            {
                if (!String.IsNullOrEmpty(programSplit[iterator]))
                {
                    syntaxChecker.addLineNumber(lineNo);

                    if (methodStored == true)
                    {
                        if (usermethods.isCommandAMethodCall(programSplit[iterator]))
                        {
                            syntaxChecker.addMessage("method has been called");
                            usermethods.methodexecution(programSplit[iterator], syntaxChecker, commandParser, graphicsArea, outputGraphics, setFill);
                            methodEnded = false;
                        }
                    }

                    if (programSplit[iterator].Contains("method") && !programSplit[iterator].Contains("end"))
                    {
                        storeMethod();
                    }

                    else if (programSplit[iterator].Contains("endmethod"))
                    {
                        syntaxChecker.addMessage("end of method block reached");
                    }

                    else if (programSplit[iterator].Contains("endwhile"))
                    {
                        syntaxChecker.addMessage("end of while block reached");
                    }

                    else if (programSplit[iterator].Contains("while"))
                    {
                        whileLoopExecution();
                    }

                    else if (programSplit[iterator].Contains("endif"))
                    {
                        syntaxChecker.addMessage("end of if block reached");
                    }

                    else if (programSplit[iterator].Contains("if") && !programSplit[iterator].Contains("end"))
                    {
                        ifExecution(programSplit[iterator]);
                    }

                    else if (var.isVariableDeclared(programSplit[iterator]))
                    {
                        var.variableExecution(programSplit[iterator], syntaxChecker);
                    }

                    else if (methodEnded == true)
                    {
                        commandExecution(programSplit[iterator]);
                    }

                    methodEnded = true;
                    lineNo++;
                }

            }
        }
        
        /// <summary>
        /// This method is designed to try each command individually and see if it will run properly.
        /// This will loop through every line and attempt to execute each command passed to it.
        /// </summary>
        public void checkProgram()
        {
            lineNo = 1;
            int pointer = 1;
            programSplit = program.Split(';');

            for (int i = 0; i < programSplit.Length; i++)
            {
                syntaxChecker.addLineNumber(pointer);

                if(!String.IsNullOrEmpty(programSplit[i]))
                {                                        
                    if (programSplit[i].Contains("method") && !programSplit[i].Contains("end"))
                    {
                        storeMethodCheck();
                    }

                    else if (programSplit[i].Contains("endmethod"))
                    {
                        syntaxChecker.addMessage("end of method block reached");
                    }

                    else if (programSplit[i].Contains("endwhile"))
                    {
                        syntaxChecker.addMessage("end of while block reached");
                    }

                    else if (programSplit[i].Contains("while"))
                    {
                        whileloopexecutioncheck();
                    }

                    else if (programSplit[i].Contains("endif"))
                    {
                        syntaxChecker.addMessage("end of if block reached");
                    }

                    else if (programSplit[i].Contains("if") && !programSplit[i].Contains("end"))
                    {
                        ifExecution(programSplit[i]);
                    }

                    else if (var.isVariableDeclared(programSplit[i]))
                    {
                        var.variableExecution(programSplit[i], syntaxChecker);
                    }

                    else 
                    {
                        commandExecution(programSplit[i]);
                    }

                    lineNo++;
                    pointer++;
                }
  
                
            }

            var.resetVariables();
        }

        /// <summary>
        /// This method is designed to store all commands declared within a method
        /// </summary>
        public void storeMethod()
        {
            if(usermethods.isEndMethodFound(program,lineNo))
            {
                syntaxChecker.addMessage("Method declared commands stored");
                usermethods.storeMethodName(programSplit[iterator]);
                usermethods.storeMethodCommands(lineNo);
                methodStored = true;
                iterator = usermethods.getLineNoOfEndMethod();
                lineNo = usermethods.getLineNoOfEndMethod() + 1;
            }

            else
            {
                syntaxChecker.addMessage("Method not ended properly method declaration will be ignored");
            }
        }

        /// <summary>
        /// This method is designed see if a method declaration is ended properly so that commands can be stored.
        /// Commands within the method would be tested as this is designed for the checking the program
        /// </summary>
        public void storeMethodCheck()
        {
            if (usermethods.isEndMethodFound(program, lineNo))
            {
                syntaxChecker.addMessage("Method declared commands would be stored");
            }

            else
            {
                syntaxChecker.addMessage("Method not ended properly method would be ignored");
            }
        }

        /// <summary>
        /// This is the method responsible for checking if a while loop would be executed. (NOTE it doesn't execute the while as its designed for checking)
        /// Depending on the checks passed it will return a message relating to if the while loop would executed. 
        /// </summary>
        /// <catches>conditionalformatexception if the while statement isn't formatted or entered correctly</catches>
        /// <catches>operatornotrecognised if the statement doesn't contain a recognised operator</catches>
        /// <catches>formatexception if the statement can't be evaluated properly</catches>
        public void whileloopexecutioncheck()
        {
            loop.setProgram(program);
            loop.setLineNoWhileFound(lineNo);

            if (loop.findEndWhile())
            {
                ifs.storeStatement(programSplit[loop.getLineWhileFound() - 1]);
                try
                {
                    ifs.setConditionals();
                }
                catch (conditionalformatexception e)
                {
                    syntaxChecker.addMessage(e.Message);
                    return;
                }

                try
                {
                    ifs.processOperator();
                }
                catch (operatornotrecognised e)
                {
                    syntaxChecker.addMessage(e.Message);
                    return;
                }

                try
                {
                    if(ifs.evaluateStatement())
                    {
                        syntaxChecker.addMessage("This condition is true. While would be executed");
                    }

                    else
                    {
                        syntaxChecker.addMessage("This condition is false. While would be skipped");
                    }
                }
                catch(FormatException e)
                {
                    syntaxChecker.addMessage(e.Message);
                    return;
                }
            }

            else
            {
                syntaxChecker.addMessage("No endwhile present this loop would be ignored");
            }

        }

        /// <summary>
        /// This is the command responsible for the execution of while loops.
        /// It will check that the while has been ended properly. 
        /// If it isn't ended properly the loop will be ignored and the commands under it will be executed.
        /// If it is ended properly, it will then check to see if the statement is true or false.
        /// If the statement is true, the loop will execute.
        /// If the statement is false, the loop will be skipped and any commands within the while block will be ignored 
        /// </summary>
        /// <catches>conditionalformatexception if the while statement isn't formatted or entered correctly</catches>
        /// <catches>operatornotrecognised if the statement doesn't contain a recognised operator</catches>
        /// <catches>formatexception if the statement can't be evaluated properly</catches>
        public void whileLoopExecution()
        {
            loop.setProgram(program);
            loop.setLineNoWhileFound(lineNo);

            if(loop.findEndWhile())
            {
                int startofloop = loop.getLineWhileFound();
                int endofloop = loop.getEndWhile();

                ifs.storeStatement(programSplit[startofloop - 1]);
                try
                {
                    ifs.setConditionals();
                }

                catch (conditionalformatexception e)
                {
                    syntaxChecker.addMessage(e.Message + " (" + programSplit[startofloop - 1] + ") ");
                    lineNo = endofloop + 1;
                    iterator = endofloop;
                    return;
                }

                try
                {
                    ifs.processOperator();
                }

                catch (operatornotrecognised e)
                {
                    syntaxChecker.addMessage(e.Message);
                    lineNo = endofloop;
                    iterator = endofloop;
                    return;
                }

                try
                {
                    if(ifs.evaluateStatement())
                    {
                        int i = startofloop;
                        syntaxChecker.addMessage("while condition is true. Executing commands");

                        while(i <= endofloop)
                        {
                            if(i == endofloop)
                            {
                                ifs.storeStatement(programSplit[startofloop - 1]);
                                ifs.setConditionals();
                                ifs.processOperator();

                                if (ifs.evaluateStatement())
                                {
                                    i = startofloop;
                                    syntaxChecker.addLineNumber(i+1);
                                }

                                else
                                {
                                    break;
                                }
                            }

                            else
                            {
                                syntaxChecker.addLineNumber(i+1);
                            }

                            if (var.isVariableDeclared(programSplit[i]))
                            {
                                var.variableExecution(programSplit[i], syntaxChecker);
                            }
                                                        
                            else
                            {
                                commandExecution(programSplit[i]);
                            }

                            i++;
                        }
                    }

                    else
                    {
                        syntaxChecker.addMessage("While condition is false. Skipping to end of while block");
                        lineNo = endofloop;
                        iterator = endofloop - 1;
                    }
                }

                catch(FormatException e)
                {
                    syntaxChecker.addMessage(e.Message);
                    lineNo = endofloop + 1;
                    iterator = endofloop;
                    return;
                }

            }

            else
            {
                syntaxChecker.addMessage("No end of loop detected loop has been ignored");
            }
        }

        /// <summary>
        /// This method handles the execution of if statements.
        /// It will check if the if statement has been ended properly.
        /// If it is ended properly it will evaluate the statement passed to it. If the statemnent is true, commands within the if block will be executed.
        /// If the statement is false, the commands will be skipped and execution will continue from the point where the if block ends.
        /// </summary>
        /// <param name="declaration">representing the if statement passed to the method</param>
        /// <catches>conditionalformatexception if the while statement isn't formatted or entered correctly</catches>
        /// <catches>operatornotrecognised if the statement doesn't contain a recognised operator</catches>
        /// <catches>formatexception if the statement can't be evaluated properly</catches>
        public void ifExecution(string declaration)
        {
            if(ifs.findendif(program,lineNo))
            {
                ifs.storeStatement(declaration);
                try
                {
                    ifs.setConditionals();
                }

                catch(conditionalformatexception e)
                {
                    syntaxChecker.addMessage(e.Message + " (" + declaration + ") ");
                    lineNo = ifs.getendif() + 1;
                    iterator = ifs.getendif() - 1;
                    return;
                }

                try
                {
                    ifs.processOperator();
                }

                catch (operatornotrecognised e)
                {
                    syntaxChecker.addMessage(e.Message + "(" + declaration + ")");
                    lineNo = ifs.getendif() + 1;
                    iterator = ifs.getendif() - 1;
                    return;
                }

                try
                {
                    if (!ifs.evaluateStatement())
                    {
                        syntaxChecker.addMessage("Statement is false, skipping to endif in the if block");
                        lineNo = ifs.getendif();
                        iterator = ifs.getendif() - 1;
                    }

                    else
                    {
                        syntaxChecker.addMessage("Statement is true, executing commands in if block");
                    }
                }

                catch (System.FormatException e)
                {
                    syntaxChecker.addMessage(e.Message);
                    iterator = ifs.getendif() - 1;
                    lineNo = ifs.getendif() + 1;
                    return;
                }
            }

            else
            {
                syntaxChecker.addMessage("No end of if detected. If declaration will be ignored.");
            }
            
        }

        /// <summary>
        /// This is the method responsible for executing the programs commands.
        /// It will take the command passed to it and store both the command and it's paramaters (if any)
        /// It will then attempt to execute a command based on whats passed to it
        /// </summary>
        /// <param name="rawCommandLine">represents the command passed to it</param>
        /// <catches>Argumentexception when the command entered does not exist</catches>
        public void commandExecution(string rawCommandLine)
        {
            commandParser.resetRawParamaters();
            commandParser.parseCommandLine(var.updateRawCommandLine(rawCommandLine.Trim()));
                        
            if(commandParser.doParamatersExist())
            {
                commandParser.setRawParamaters();
            }

            if(commandParser.getTypedCommand().ToLower().Equals("setfill"))
            {
                setFill.setFillExecution(commandParser,syntaxChecker);
            }

            else
            {
                try
                {
                    Commands newCommand = factory.getCommands(commandParser.getTypedCommand(), syntaxChecker);
                    newCommand.executeCommand(commandParser, graphicsArea, outputGraphics, setFill, syntaxChecker);
                }
                
                catch(System.ArgumentException e)
                {
                    syntaxChecker.addMessage(e.Message);
                }
                
            }

        }

    }
}