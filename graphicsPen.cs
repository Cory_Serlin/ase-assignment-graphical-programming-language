﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class represents the pen that the user uses to draw onto the output graphics.
    /// It has a pen for drawing shapes and a brush for filling them in.
    /// </summary>
    public class graphicsPen
    {
        // Instance data for the pen and it's position
        private Pen myPen;
        private int xPos, yPos;
        private Graphics graphicPanel;
        private Brush myBrush;

        /// <summary>
        /// This is the constructor for the GraphicsArea. 
        /// It sets the current graphics context to be what the user sees in the DrawingArea
        /// It sets the initial position of the pen (0,0)
        /// It sets the starting properties of the pen object
        /// It sets the starting properties of the brush object
        /// By default, isFillOn is set to false
        /// </summary>
        /// <param name="GraphicPanel"> this represents the current graphic context that the user sees</param>
        public graphicsPen(Graphics GraphicPanel)
        {
            this.graphicPanel = GraphicPanel;
            xPos = yPos = 0;
            myPen = new Pen(Color.Black, 1);
            myBrush = new SolidBrush(myPen.Color);
        }

        /// <summary>
        /// Sets the X position of the Pen
        /// </summary>
        /// <param name="toX">The X position the user wants to set the pen to</param>
        public void setPenX(int toX)
        {
            this.xPos = toX;
        }

        /// <summary>
        /// Sets the Y position of the Pen
        /// </summary>
        /// <param name="toY">The Y position the user wants to set the pen to</param>
        public void setPenY(int toY)
        {
            this.yPos = toY;
        }

        /// <summary>
        /// This returns the current X position of the Pen
        /// </summary>
        /// <returns>The current X co-ordinate of the pen</returns>
        public int getPenX()
        {
            return xPos;
        }

        /// <summary>
        /// This returns the current Y position of the Pen
        /// </summary>
        /// <returns>The current Y co-ordinate of the pen</returns>
        public int getPenY()
        {
            return yPos;
        }

        /// <summary>
        /// This methods sets the pen colour when a valid colour is passed to it
        /// </summary>
        /// <param name="Colour">The colour the user picks</param>
        public void setPenBrush(string Colour)
        {
            myPen = new Pen(Color.FromName(Colour), 1);
            myBrush = new SolidBrush(myPen.Color);
        }

        /// <summary>
        /// This is a method to get the current context of the pen
        /// </summary>
        /// <returns>The pen used for drawing</returns>
        public Pen GetPen()
        {
            return myPen;
        }

        /// <summary>
        /// This is a method that gets the current context of the brush
        /// </summary>
        /// <returns>The brush for filling shapes</returns>
        public Brush GetBrush()
        {
            return myBrush;
        }



    }
}
