﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class represents a conditional format exception.
    /// This exception will be thrown when a condition declaration (e.g. if x = 100;) is inputted incorrectly.
    /// This exception will also be thrown if part of the condition declaration is missing.
    /// </summary>
    public class conditionalformatexception : Exception
    {
            /// <summary>
            /// Constructor for the custom exception
            /// </summary>
            public conditionalformatexception()
            {
            }

            /// <summary>
            /// Constructor for the custom exception
            /// </summary>
            /// <param name="message">error message displayed to the user</param>
            public conditionalformatexception(string message)
                : base(message)
            {
            }


            public conditionalformatexception(string message, Exception inner)
                : base(message, inner)
            {
            }
           
    }
}
