﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{    
    /// <summary>
    /// This is the implementation of the Factory Pattern.
    /// This creates instances of a command to execute based on a given command.
    /// </summary>
    public class commandFactory
    {
        /// <summary>
        /// This class creates an instance of a command when a command is passed to it.
        /// </summary>
        /// <param name="commandType">Representing the command the user wants to execute</param>
        /// <param name="syntax">This is the handler for passing messages to it</param>
        /// <returns>An instance of a command</returns>
        /// <throws>Argument exception when the command doesn't exist</throws>
        public Commands getCommands(String commandType, messageStorer syntax)
        {            
            switch(commandType.ToLower())
            {
                case "drawrectangle":
                    return new drawRectangle();

                case "moveto":
                    return new moveTo();

                case "setpencolour":
                    return new setPenColour();

                case "drawcircle":
                        return new drawCircle();

                case "drawtriangle":
                    return new drawTriangle();

                case "drawto":
                    return new drawTo();

                case "clearpanel":
                    return new clearPanel();

                case "resetpanel":
                    return new resetPanel();

                default:
                    System.ArgumentException argEx = new System.ArgumentException("Factory error: " + commandType + " does not exist");
                    throw argEx;

            }
          
        }
    
    
    
    }
}
