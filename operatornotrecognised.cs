﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class represents a operationnotfound exception.
    /// This exception will be thrown when an operator in a conditional statement isn't recognised 
    /// </summary>
    class operatornotrecognised : Exception
    {
        public operatornotrecognised()
        {
        }

        public operatornotrecognised(string message)
            : base(message)
        {
        }

        public operatornotrecognised(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
