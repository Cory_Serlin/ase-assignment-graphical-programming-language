﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is the class that handles variables 
    /// </summary>
    public class variables
    {
        //syntax
        private messageStorer syntaxChecker;

        //storage of all varNames and varValues
        private List<string> varNames = new List<string>();
        private List<string> varValues = new List<string>();

        //split for variable declaration 
        private string[] variableLineSplit;

        /// <summary>
        /// This is the constructor for the 
        /// </summary>
        /// <param name="syntaxChecker">messagestoreer created on launch of the program</param>
        public variables(messageStorer syntaxChecker)
        {
            this.syntaxChecker = syntaxChecker;
        }

        ///<summary>
        ///This will reset the varNames and varValues when necessary
        ///</summary>
        public void resetVariables()
        {
            varNames.Clear();
            varValues.Clear();
        }


        /// <summary>
        /// This method handles the declaration of a variable
        /// </summary>
        /// <param name="variableDeclaration"></param>
        /// <param name="syntax"></param>
        /// <catches>EvaluateException if the variable passed is an equation that can't be computed</catches>
        public void variableExecution(string variableDeclaration, messageStorer syntax)
        {
            splitVariable(variableDeclaration);

            if(String.IsNullOrEmpty(variableLineSplit[0].Trim()))
            {
                syntax.addMessage("No variable has been declared");
                return;
            }

            if (String.IsNullOrEmpty(variableLineSplit[1].Trim()))
            {
                syntax.addMessage("No value has been declared");
                return;
            }

            try
            {
                if (doesVarNameExist(variableLineSplit[0].Trim()))
                {
                    replaceValue(variableLineSplit[1]);
                }

                else
                {
                    storeVariable(syntaxChecker);
                }
            }

            catch (System.Data.EvaluateException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        /// <summary>
        /// This replaces any reconised var names and computes expressions
        /// </summary>
        /// <param name="varValue">representing the value of the variable the user has passed to it</param>
        public void replaceValue(string varValue)
        {
            if(!String.Equals(varValue, varValues[getVarNameIndex(variableLineSplit[0].Trim())]))
            {
                int index = getVarNameIndex(variableLineSplit[0].Trim());

                variableLineSplit[1] = updateValue(variableLineSplit[1]);

                if(variableLineSplit[1].Contains('+') || variableLineSplit[1].Contains('-') || variableLineSplit[1].Contains('*') || variableLineSplit[1].Contains('/'))
                {
                    variableLineSplit[1] = calculateExpression(variableLineSplit[1]);
                }

                varValues[index] = variableLineSplit[1];
                syntaxChecker.addMessage("Variable has been replaced");
            }

            else
            {
                Console.WriteLine("Variables are equal");
            }

        }

        /// <summary>
        /// This class replaces any part where the string contains a known variable and replaces it with a value
        /// </summary>
        /// <param name="valueToUpdate">this can be any command or statement where a variable is involved</param>
        /// <returns>the updated value</returns>
        public string updateValue(string valueToUpdate)
        {            
            if (valueToUpdate.Contains(variableLineSplit[0]))
            {
                valueToUpdate = valueToUpdate.Trim().Replace(variableLineSplit[0].Trim(), varValues[getVarNameIndex(variableLineSplit[0].Trim())]);
            }
                        
            return valueToUpdate;
        }

        /// <summary>
        /// This stores the variables
        /// </summary>
        /// <param name="syntax">repsenting the message handler</param>
        public void storeVariable(messageStorer syntax)
        {
            varNames.Add(variableLineSplit[0].Trim());
            varValues.Add(variableLineSplit[1].Trim());
            syntax.addMessage("Variable Stored");
        }

        /// <summary>
        /// This checks for a variable declaration
        /// </summary>
        /// <param name="commandInput">represents the variable declaration</param>
        /// <returns></returns>
        public Boolean isVariableDeclared(string commandInput)
        {
            if (commandInput.Trim().Contains("="))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method computes expressions
        /// </summary>
        /// <param name="varValue">represents the equation being processed</param>
        /// <returns>a string representing the computed equation</returns>
        public string calculateExpression(string varValue)
        {
            DataTable dt = new DataTable();
            int computed_value = (int)dt.Compute(varValue, "");                       
            return computed_value.ToString();
        }

        /// <summary>
        /// this checks if an element exists in a array
        /// </summary>
        /// <param name="searched_name">the name of a stored variable</param>
        /// <returns>true if the variable exists</returns>
        public Boolean doesVarNameExist(string searched_name)
        {
            return varNames.Contains(searched_name);
        }

        ///<summary>
        ///a check that returns the index number of the named variable
        ///</summary>
        /// <param name="searched_name">the name of a stored variable</param>
        public int getVarNameIndex(string searched_name)
        {
            int i = varNames.FindIndex(a => a.Contains(searched_name));
            return i;
        }

        /// <summary>
        /// method to split the variable declaration 
        /// </summary>
        /// <param name="userInput">represents the variable declaration</param>
        public void splitVariable(string userInput)
        {
            variableLineSplit = userInput.Trim().Split('=');
        }

        /// <summary>
        /// method to store the variable name 
        /// </summary>
        public void addVarName()
        {
            varNames.Add(variableLineSplit[0].Trim());
        }

        /// <summary>
        /// method to store the variable value
        /// </summary>
        public void addVarValue()
        {
            varValues.Add(variableLineSplit[1].TrimStart(' '));
        }

        /// <summary>
        /// method to get the varName
        /// </summary>
        /// <param name="indexNumber">the index where the paramater can be found</param>
        /// <returns>the name of a stored variable</returns>
        public string getVarNames(int indexNumber)
        {
            return varNames[indexNumber];
        }

        /// <summary>
        /// method to get the varValue
        /// </summary>
        /// <param name="indexNumber">the index where the paramater can be found</param>
        /// <returns>the name of a stored variable</returns>
        public string getVarValues(int indexNumber)
        {
            return varValues[indexNumber];
        }

        /// <summary>
        /// method to replace Vars declared in a command with its value
        /// </summary>
        /// <param name="rawCommandLine">the command line</param>
        /// <returns>a string representing the command with the updated values</returns>
        public string updateRawCommandLine(string rawCommandLine)
        {
            String processCommandLine = rawCommandLine;
            
            for(int i = 0; i < varNames.Count; i++)
            {               
                if(rawCommandLine.Contains(varNames[i]))
                {
                    processCommandLine = rawCommandLine.Replace(varNames[i].Trim(), varValues[i].Trim());
                }
            }
                        
            return processCommandLine;
        }

    }
}
