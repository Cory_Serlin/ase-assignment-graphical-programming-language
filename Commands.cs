﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is an abstract class that implements the interface relating to the commands that the user can execute.
    /// </summary>
    public abstract class Commands : iCommands
    {
        String[] paramaters;

        /// <summary>
        /// This is an abstract method that relates to the execution of a command.
        /// It is abstract as commands is too generic to have a specific execution.
        /// </summary>
        /// <param name="parser">The command parser used for command execution</param>
        /// <param name="pen">The graphical pen drawing on the graphics context</param>
        /// <param name="g">The graphical context of the drawing area</param>
        /// <param name="fill">The setFill command</param>
        /// <param name="syntax">The message handler that outputs any error messages</param>
        public abstract void executeCommand(commandParser parser, graphicsPen pen, Graphics g, setFill fill, messageStorer syntax);

        /// <summary>
        /// This is a check that a command object can call if the paramaters that need to be checked for validity are meant to be all ints.
        /// </summary>
        /// <returns>true if all paramaters entered are valid</returns>
        /// <returns>false if one of the paramaters entered are invalid</returns>
        public virtual bool isParamatersValid()
        {
            int invalidInt = 0;
            Boolean checkPassed = false;

            for (int i = 0; i < paramaters.Length; i++)
            {
                checkPassed = int.TryParse(paramaters[i], out invalidInt);

                if (checkPassed == false)
                {
                    break;
                }
            }

            return checkPassed;
        }

        /// <summary>
        /// This is a check that commands can call to check if the number of paramaters entered are correct.
        /// </summary>
        /// <param name="requiredNumber">The defined number of paramaters needed for the command to execute</param>
        /// <returns>true if the correct number of paramaters are present</returns>
        public virtual bool isValidNoOfParamaters(int requiredNumber)
        {
            if (paramaters.Length == requiredNumber)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// A method that commands can call for splitting the raw paramaters into an array.
        /// </summary>
        /// <param name="rawParamaters">Represents the raw paramaters the user enters</param>
        public void setParamaters(string rawParamaters)
        {
            paramaters = rawParamaters.Trim().Split(',');            
        }

        /// <summary>
        /// It gets the paramater array that the user enters
        /// </summary>
        /// <returns>an array representing the paramaters entered</returns>
        public string[] getParamaters()
        {
            return paramaters;
        }
    }
}
