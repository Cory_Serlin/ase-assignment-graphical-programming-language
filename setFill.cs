﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This class handles the set fill command
    /// </summary>
    public class setFill
    {
        private string paramater;
        private Boolean isFillOn;
    
        /// <summary>
        /// This is the constructor for the set fill command. It initially sets it to off.
        /// </summary>
        public setFill()
        {
            isFillOn = false;
        }
    
        /// <summary>
        /// This sets the paramater passed to it
        /// </summary>
        /// <param name="parser">The command parser</param>
        public void setParamater(commandParser parser)
        {
            paramater = parser.getRawParamaters();
        }

        /// <summary>
        /// This checks to ensure that only one paramater has been passed to it
        /// </summary>
        /// <param name="paramater">the paramaters passed to it</param>
        /// <returns></returns>
        public bool checkParamaterSize(string paramater)
        {
            if (paramater.Contains(','))
            {
                return false;
            }

            else
            {
                return true;
            }
        }

        /// <summary>
        /// This sets the fill to be on
        /// </summary>
        public void setFillOn()
        {
            isFillOn = true;
        }

        /// <summary>
        /// This sets the fill to be off
        /// </summary>
        public void setFillOff()
        {
            isFillOn = false;
        }

        /// <summary>
        /// This gets the current status of the setFill command (whether it is on or off)
        /// </summary>
        /// <returns>a boolean representing the status of the set fill command</returns>
        public Boolean getFillStatus()
        {
            return isFillOn;
        }

        /// <summary>
        /// This checks that the paramater entered in the command is correct
        /// </summary>
        /// <param name="option">represents the option the user selects</param>
        /// <returns>true if the option is on or off</returns>
        public Boolean isOptionValid(string option)
        {
            if (option.Equals("on", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            else if (option.Equals("off", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// This method handles the execution of the set fill command.
        /// </summary>
        /// <param name="parser"></param>
        /// <param name="syntax"></param>
        /// <catches>NullReferenceException if the user does not enter any paramaters</catches>
        public void setFillExecution(commandParser parser, messageStorer syntax)
        {
            try
            {
                setParamater(parser);

                if (checkParamaterSize(paramater))
                {
                    if (isOptionValid(paramater))
                    {
                        if (paramater.ToLower().Equals("on"))
                        {
                            setFillOn();
                            syntax.addMessage("Fill is on");
                        }

                        else if (paramater.ToLower().Equals("off"))
                        {
                            setFillOff();
                            syntax.addMessage("Fill is off");
                        }
                    }

                    else
                    {
                        syntax.addMessage("The option you have entered is invalid");
                    }
                }

                else
                {
                    syntax.addMessage("invalid number of params (1)");
                }

            }

            catch(System.NullReferenceException)
            {
                syntax.addMessage("No paramaters have been entered");
            }

        }



    }
}
