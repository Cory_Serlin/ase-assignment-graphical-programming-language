﻿
namespace ASE_Assignment___Graphical_Programming_Language
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.programAreaLabel = new System.Windows.Forms.Label();
            this.runProgramButton = new System.Windows.Forms.Button();
            this.checkProgramButton = new System.Windows.Forms.Button();
            this.commandLineLabel = new System.Windows.Forms.Label();
            this.commandLineTextBox = new System.Windows.Forms.TextBox();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToUseTheProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfCommandsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.consoleWindowLabel = new System.Windows.Forms.Label();
            this.consoleWindowTextBox = new System.Windows.Forms.TextBox();
            this.drawingAreaPictureBox = new System.Windows.Forms.PictureBox();
            this.drawingAreaLabel = new System.Windows.Forms.Label();
            this.programAreaTextBox = new System.Windows.Forms.RichTextBox();
            this.mainMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drawingAreaPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // programAreaLabel
            // 
            this.programAreaLabel.AutoSize = true;
            this.programAreaLabel.Location = new System.Drawing.Point(12, 38);
            this.programAreaLabel.Name = "programAreaLabel";
            this.programAreaLabel.Size = new System.Drawing.Size(71, 13);
            this.programAreaLabel.TabIndex = 1;
            this.programAreaLabel.Text = "Program Area";
            // 
            // runProgramButton
            // 
            this.runProgramButton.Location = new System.Drawing.Point(15, 397);
            this.runProgramButton.Name = "runProgramButton";
            this.runProgramButton.Size = new System.Drawing.Size(81, 23);
            this.runProgramButton.TabIndex = 3;
            this.runProgramButton.Text = "Run Program";
            this.runProgramButton.UseVisualStyleBackColor = true;
            this.runProgramButton.Click += new System.EventHandler(this.runProgramButton_Click);
            // 
            // checkProgramButton
            // 
            this.checkProgramButton.Location = new System.Drawing.Point(102, 397);
            this.checkProgramButton.Name = "checkProgramButton";
            this.checkProgramButton.Size = new System.Drawing.Size(91, 23);
            this.checkProgramButton.TabIndex = 4;
            this.checkProgramButton.Text = "Check Program";
            this.checkProgramButton.UseVisualStyleBackColor = true;
            this.checkProgramButton.Click += new System.EventHandler(this.checkProgramButton_Click);
            // 
            // commandLineLabel
            // 
            this.commandLineLabel.AutoSize = true;
            this.commandLineLabel.Location = new System.Drawing.Point(12, 432);
            this.commandLineLabel.Name = "commandLineLabel";
            this.commandLineLabel.Size = new System.Drawing.Size(77, 13);
            this.commandLineLabel.TabIndex = 5;
            this.commandLineLabel.Text = "Command Line";
            // 
            // commandLineTextBox
            // 
            this.commandLineTextBox.Location = new System.Drawing.Point(15, 448);
            this.commandLineTextBox.Name = "commandLineTextBox";
            this.commandLineTextBox.Size = new System.Drawing.Size(446, 20);
            this.commandLineTextBox.TabIndex = 6;
            this.commandLineTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLineTextBox_KeyDown);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveProgramToolStripMenuItem,
            this.loadProgramToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveProgramToolStripMenuItem
            // 
            this.saveProgramToolStripMenuItem.Name = "saveProgramToolStripMenuItem";
            this.saveProgramToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.saveProgramToolStripMenuItem.Text = "Save Program";
            this.saveProgramToolStripMenuItem.Click += new System.EventHandler(this.saveProgramToolStripMenuItem_Click);
            // 
            // loadProgramToolStripMenuItem
            // 
            this.loadProgramToolStripMenuItem.Name = "loadProgramToolStripMenuItem";
            this.loadProgramToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.loadProgramToolStripMenuItem.Text = "Load Program";
            this.loadProgramToolStripMenuItem.Click += new System.EventHandler(this.loadProgramToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howToUseTheProgramToolStripMenuItem,
            this.listOfCommandsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // howToUseTheProgramToolStripMenuItem
            // 
            this.howToUseTheProgramToolStripMenuItem.Name = "howToUseTheProgramToolStripMenuItem";
            this.howToUseTheProgramToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.howToUseTheProgramToolStripMenuItem.Text = "How to use the program";
            this.howToUseTheProgramToolStripMenuItem.Click += new System.EventHandler(this.howToUseTheProgramToolStripMenuItem_Click);
            // 
            // listOfCommandsToolStripMenuItem
            // 
            this.listOfCommandsToolStripMenuItem.Name = "listOfCommandsToolStripMenuItem";
            this.listOfCommandsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.listOfCommandsToolStripMenuItem.Text = "List of commands";
            this.listOfCommandsToolStripMenuItem.Click += new System.EventHandler(this.listOfCommandsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programInformationToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // programInformationToolStripMenuItem
            // 
            this.programInformationToolStripMenuItem.Name = "programInformationToolStripMenuItem";
            this.programInformationToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.programInformationToolStripMenuItem.Text = "Program Information";
            this.programInformationToolStripMenuItem.Click += new System.EventHandler(this.programInformationToolStripMenuItem_Click);
            // 
            // mainMenuStrip1
            // 
            this.mainMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.mainMenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip1.Name = "mainMenuStrip1";
            this.mainMenuStrip1.Size = new System.Drawing.Size(1251, 24);
            this.mainMenuStrip1.TabIndex = 0;
            this.mainMenuStrip1.Text = "mainMenuStrip";
            // 
            // consoleWindowLabel
            // 
            this.consoleWindowLabel.AutoSize = true;
            this.consoleWindowLabel.Location = new System.Drawing.Point(12, 482);
            this.consoleWindowLabel.Name = "consoleWindowLabel";
            this.consoleWindowLabel.Size = new System.Drawing.Size(87, 13);
            this.consoleWindowLabel.TabIndex = 7;
            this.consoleWindowLabel.Text = "Console Window";
            // 
            // consoleWindowTextBox
            // 
            this.consoleWindowTextBox.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.consoleWindowTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consoleWindowTextBox.ForeColor = System.Drawing.Color.Red;
            this.consoleWindowTextBox.Location = new System.Drawing.Point(16, 502);
            this.consoleWindowTextBox.Multiline = true;
            this.consoleWindowTextBox.Name = "consoleWindowTextBox";
            this.consoleWindowTextBox.ReadOnly = true;
            this.consoleWindowTextBox.Size = new System.Drawing.Size(1223, 135);
            this.consoleWindowTextBox.TabIndex = 8;
            // 
            // drawingAreaPictureBox
            // 
            this.drawingAreaPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawingAreaPictureBox.Location = new System.Drawing.Point(487, 54);
            this.drawingAreaPictureBox.Name = "drawingAreaPictureBox";
            this.drawingAreaPictureBox.Size = new System.Drawing.Size(735, 428);
            this.drawingAreaPictureBox.TabIndex = 9;
            this.drawingAreaPictureBox.TabStop = false;
            this.drawingAreaPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.drawingAreaPictureBox_Paint);
            // 
            // drawingAreaLabel
            // 
            this.drawingAreaLabel.AutoSize = true;
            this.drawingAreaLabel.Location = new System.Drawing.Point(484, 38);
            this.drawingAreaLabel.Name = "drawingAreaLabel";
            this.drawingAreaLabel.Size = new System.Drawing.Size(71, 13);
            this.drawingAreaLabel.TabIndex = 10;
            this.drawingAreaLabel.Text = "Drawing Area";
            // 
            // programAreaTextBox
            // 
            this.programAreaTextBox.Location = new System.Drawing.Point(16, 55);
            this.programAreaTextBox.Name = "programAreaTextBox";
            this.programAreaTextBox.Size = new System.Drawing.Size(445, 336);
            this.programAreaTextBox.TabIndex = 11;
            this.programAreaTextBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1251, 643);
            this.Controls.Add(this.programAreaTextBox);
            this.Controls.Add(this.drawingAreaLabel);
            this.Controls.Add(this.drawingAreaPictureBox);
            this.Controls.Add(this.consoleWindowTextBox);
            this.Controls.Add(this.consoleWindowLabel);
            this.Controls.Add(this.commandLineTextBox);
            this.Controls.Add(this.commandLineLabel);
            this.Controls.Add(this.checkProgramButton);
            this.Controls.Add(this.runProgramButton);
            this.Controls.Add(this.programAreaLabel);
            this.Controls.Add(this.mainMenuStrip1);
            this.MainMenuStrip = this.mainMenuStrip1;
            this.Name = "Form1";
            this.Text = "ASE Assignment - Graphical Programming Language";
            this.mainMenuStrip1.ResumeLayout(false);
            this.mainMenuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drawingAreaPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label programAreaLabel;
        private System.Windows.Forms.Button runProgramButton;
        private System.Windows.Forms.Button checkProgramButton;
        private System.Windows.Forms.Label commandLineLabel;
        private System.Windows.Forms.TextBox commandLineTextBox;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToUseTheProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfCommandsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programInformationToolStripMenuItem;
        private System.Windows.Forms.MenuStrip mainMenuStrip1;
        private System.Windows.Forms.Label consoleWindowLabel;
        private System.Windows.Forms.TextBox consoleWindowTextBox;
        private System.Windows.Forms.PictureBox drawingAreaPictureBox;
        private System.Windows.Forms.Label drawingAreaLabel;
        private System.Windows.Forms.RichTextBox programAreaTextBox;
    }
}

