﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment___Graphical_Programming_Language
{
    /// <summary>
    /// This is the class that deals with the execution of the clearPanel command
    /// </summary>
    public class clearPanel : Commands
    {
        /// <summary>
        /// This is the method to execute the clearPanel command.
        /// It checks to ensure that there aren't any paramaters entered in the command.
        /// If it passes that check, the command executes.
        /// If the check fails, then it produces an error relating to the check that fails.
        /// </summary>
        /// <param name="parser">The command parser used for obtaining the command passed to it</param>
        /// <param name="pen">The graphical pen drawing on the graphics context</param>
        /// <param name="g">The graphical context of the drawing area</param>
        /// <param name="fill">The setFill command</param>
        /// <param name="syntax">The message handler that outputs any error messages</param>
        public override void executeCommand(commandParser parser, graphicsPen pen, Graphics g, setFill fill, messageStorer syntax)
        {
            if(isValidNoOfParamaters(parser))
            {
                g.Clear(Color.White);
                syntax.addMessage("The entered command (ClearPanel) is valid.");
            }

            else
            {
                syntax.addMessage("This command doesn't require a paramater. Please remove the paramater and try again.");
            }
        }

        /// <summary>
        /// This class checks if any paramaters have been entered into the command.
        /// </summary>
        /// <param name="parser">The command parser</param>
        /// <returns>true if there are no paramaters present</returns>
        /// <returns>false if paramaters have been entered</returns>
        public bool isValidNoOfParamaters(commandParser parser)
        {
            if(parser.doParamatersExist())
            {
                return false;
            }

            else
            {
                return true;
            }                
        }
    }
}
